﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbstractCharacterController : MonoBehaviour
{
    protected Rigidbody2D rbody2D;
    // 静的パラメータ
    [SerializeField] protected float groundSpeed = 0;
    [SerializeField] protected float airSpeed = 0;
    [SerializeField] protected float jumpForce = 2f;
    [SerializeField] protected float jumpContinueTime = 0.17f;

    // 動的パラメータ
    protected enum AnimationStatus : byte {
        Wait = 0,
        Run = 1,
        Jump = 3,
        Fall = 4,
        Attack = 5
    }

    protected AnimationStatus status;
    protected bool statusLocked = false;
    protected bool onGround = true;
    protected string xVector = "r";
    protected int deltaFrame = 0;
    protected int comboCount = 0;
    protected bool isFall = false;

    // Start is called before the first frame update
    protected void Start()
    {
        this.rbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    protected void Update()
    {
        
    }
}
