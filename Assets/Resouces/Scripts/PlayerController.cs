﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerController : AbstractCharacterController {
    private PhysicsMaterial2D physics;
    // 静的パラメータ

    [SerializeField] private float jumpContinueForce = 4;
    [SerializeField] private GameObject Animation = null;
    [SerializeField] private int[] attackFrame = new int[2] { 19, 11 };
    [SerializeField] private int[] comboReceptFrame = new int[3] { 0, 10, 0 };
    [SerializeField] private int maxCombo = 2;
    //[SerializeField] private float onGroundFliction = 0.0f;
    //[SerializeField] private float airFliction = 0;
    [SerializeField] private float slideCoefficient = 0;
    [SerializeField] private float slideJumpCoefficient = 0;
    // 動的パラメータ

    private float jumpTime = 0;

    // Start is called before the first frame update
    new void Start() {
        base.Start();
        CapsuleCollider2D clider = GetComponent<CapsuleCollider2D>();
        this.physics = clider.sharedMaterial;
    }

    // Update is called once per frame
    void Update() {
        // 動的パラメータ更新
        this.isFall = this.rbody2D.velocity.y <= -0.2f && !this.onGround;
        Debug.Log("onGround:" + this.onGround);
        //this.physics.friction = this.onGround ? this.onGroundFliction : this.airFliction;
        Debug.Log("friction:" + this.physics.friction);

        if (this.jumpTime > 0) {
            jumpTime =- Time.deltaTime;
        }
        // コンボ中処理
        if (this.deltaFrame <= 1) {
            this.comboCount = 0;
        }
        if (this.comboCount > 0) {
            this.deltaFrame--;
        }

        // 動作処理
        bool rightPushed = Input.GetKey(KeyCode.RightArrow);
        bool leftPushed = Input.GetKey(KeyCode.LeftArrow);
        float horiAxis = 0f;
        if (rightPushed && !leftPushed) {
            horiAxis = 1;
        } else if (!rightPushed && leftPushed) {
            horiAxis = -1;
        }
        if (!this.statusLocked) {
            this.run(horiAxis);      
        }

        bool zPushed = Input.GetKeyDown(KeyCode.Z);
        if (Input.GetKey(KeyCode.Space)) {
            this.jump();
        }
        if (zPushed) {
            this.attack(horiAxis);
        } else if ((rightPushed || leftPushed) && this.onGround && !(rightPushed && leftPushed) && !this.statusLocked) {
            string cVector = this.xVector;
            float x = this.rbody2D.velocity.x;
            if (x > 0f) {
                cVector = "r";
            }
            else if (x < 0) {
                cVector = "l";
            }
            this.changeAnimation(AnimationStatus.Run, cVector);
        }
        else if (this.isFall) {
            this.changeAnimation(AnimationStatus.Fall, this.xVector);
        }
        else if (!this.onGround) {
            this.changeAnimation(AnimationStatus.Jump, this.xVector);
        }
        else {
            this.changeAnimation(AnimationStatus.Wait, this.xVector);
        }
        if (zPushed) {
            bool isCancel = (this.deltaFrame < this.comboReceptFrame[comboCount - 1]) && (this.deltaFrame > 0);
            this.changeAnimation(AnimationStatus.Attack, this.xVector, isCancel);
        }
    }
    private void run(float axis) {
        bool rightPushed = Input.GetKey(KeyCode.RightArrow);
        bool leftPushed = Input.GetKey(KeyCode.LeftArrow);
        if (rightPushed && leftPushed) {
            this.rbody2D.velocity = new Vector2(0, this.rbody2D.velocity.y);
        }
        else {
            float speed = this.onGround ? this.groundSpeed : this.airSpeed;
            this.rbody2D.velocity = new Vector2(axis * speed, this.rbody2D.velocity.y);
        }
    }
    private void jump() {
        if (!this.isFall && this.onGround) {
            this.rbody2D.AddForce(Vector2.up * this.jumpForce, ForceMode2D.Impulse);
            this.onGround = false;
            this.jumpTime = jumpContinueTime;
        }
        else if (this.jumpTime > 0 && !this.onGround) {
            this.rbody2D.AddForce(Vector2.up * jumpContinueForce);
        }
    }
    private void attack(float axis) {
        if (this.comboCount < this.maxCombo && this.comboCount == 0) {
            this.comboCount++;
            this.deltaFrame = this.attackFrame[this.comboCount - 1];
            float speed = this.onGround ? this.groundSpeed : this.airSpeed;
            float jump = this.onGround ? this.jumpForce * this.slideJumpCoefficient * Mathf.Abs(axis): 0;
            float slide = this.jumpForce * this.slideCoefficient * axis;
            this.rbody2D.AddForce(new Vector2(slide, jump), ForceMode2D.Impulse);
            this.onGround = false;
        } else if (this.comboCount < this.maxCombo && this.deltaFrame <= this.comboReceptFrame[this.comboCount - 1] && this.comboCount == 1) {
            this.comboCount++;
            this.deltaFrame = this.attackFrame[this.comboCount - 1];
        }

    }
    private void changeAnimation(AnimationStatus status, string xVector, bool isCancelCommand = false) {
        Script_SpriteStudio6_Root root = Script_SpriteStudio6_Root.Parts.RootGet(Animation, true);

        // 
        bool isForce = (xVector != this.xVector && status == AnimationStatus.Run) || (status != AnimationStatus.Run && status != this.status);

        // statusが変更されている or 走る向きの変更がある
        // and
        // ロックがかかっていない or キャンセルコマンド
        if ((status != this.status || isForce) && (!this.statusLocked || isCancelCommand)) {
            this.status = status;
            string name = null;
            int loop = 0;

            if (this.status == AnimationStatus.Run ) {
                this.xVector = xVector;
                name = "run";
                loop = 0;
            } else if (this.status == AnimationStatus.Wait) {
                name = "wait";
                loop = 0;
            } else if (this.status == AnimationStatus.Jump) {
                name = "jump";
                loop = 1;
            } else if (this.status == AnimationStatus.Fall) {
                name = "fall";
                loop = 1;
            } else if (this.status == AnimationStatus.Attack) {
                name = "attack" + this.comboCount;
                loop = 1;
                this.statusLocked = true;
            }
            name = xVector + name;
            int index = root.IndexGetAnimation(name);
            bool isSuccess = root.AnimationPlay(-1, index, loop);
            if (this.statusLocked) {
                root.FunctionPlayEnd = this.lockOff;
            }
        }
    }
    private bool lockOff(Script_SpriteStudio6_Root InstanceRoot, GameObject ObjectControl) {
        this.statusLocked = false;
        return true;
    }
    void OnTriggerStay2D(Collider2D collision) {
        if (collision.CompareTag("Terrain")) {
            this.onGround = true;
        }
    }
}
